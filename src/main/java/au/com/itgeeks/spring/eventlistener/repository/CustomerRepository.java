package au.com.itgeeks.spring.eventlistener.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import au.com.itgeeks.spring.eventlistener.model.Customer;

/*
 * 
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
  
}
