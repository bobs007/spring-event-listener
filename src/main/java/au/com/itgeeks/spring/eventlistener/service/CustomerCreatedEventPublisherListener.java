package au.com.itgeeks.spring.eventlistener.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import au.com.itgeeks.spring.eventlistener.events.CustomerCreatedEvent;

@Component
public class CustomerCreatedEventPublisherListener {

  public static final Logger logger =
      LoggerFactory.getLogger(CustomerCreatedEventPublisherListener.class);

  @EventListener
  void handle(CustomerCreatedEvent event) {
    logger.info("'{}' handling todo '{}'", Thread.currentThread(), event.getCustomer().getId());
  }
}
