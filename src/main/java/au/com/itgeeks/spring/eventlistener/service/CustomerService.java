package au.com.itgeeks.spring.eventlistener.service;

import au.com.itgeeks.spring.eventlistener.events.CustomerCreatedEvent;
import au.com.itgeeks.spring.eventlistener.model.Customer;
import au.com.itgeeks.spring.eventlistener.repository.CustomerRepository;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * 
 * @author bobs
 *
 */
@Service
public class CustomerService {

  private final CustomerRepository customerRepository;
  private final ApplicationEventPublisher applicationEventPublisher;

  public CustomerService(CustomerRepository customerRepository,
      ApplicationEventPublisher applicationEventPublisher) {
    this.customerRepository = customerRepository;
    this.applicationEventPublisher = applicationEventPublisher;
  }

  @Transactional
  public Customer createCustomer(String name, String email) {
    final Customer newCustomer = customerRepository.save(new Customer(name, email));
    final CustomerCreatedEvent event = new CustomerCreatedEvent(newCustomer);
    applicationEventPublisher.publishEvent(event);
    return newCustomer;
  }
}
