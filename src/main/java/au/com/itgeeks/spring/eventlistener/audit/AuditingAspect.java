package au.com.itgeeks.spring.eventlistener.audit;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import au.com.itgeeks.spring.eventlistener.audit.Auditable.AuditType;

@Aspect
@Component
public class AuditingAspect {

  public static final Logger logger = LoggerFactory.getLogger(AuditingAspect.class);

  /**
   * 
   * @param auditable
   */
  @Pointcut("@annotation(auditable)")
  public void annotationPointCutDefinition(Auditable auditable) {}


  /**
   * 
   */
  @Pointcut("execution(* *(..))")
  public void atExecution() {}

  /**
   * 
   * @param joinPoint
   * @param entity
   * @throws Throwable
   */
  @AfterThrowing(value = "atExecution()", throwing = "e")
  public void logMethodCall(JoinPoint joinPoint, Throwable e) throws Throwable {
    logger.info("inside logAuditActivityToFile {}", e);
  }

  /**
   * 
   * @param joinPoint
   * @param entity
   * @throws Throwable
   */
  @AfterReturning(value = "annotationPointCutDefinition(auditable) && atExecution()",
      returning = "entity")
  public void logMethodCall(JoinPoint joinPoint, Object entity) throws Throwable {
    if (isAuditType(getAuditableObject(joinPoint), AuditType.PERSISTENCE_TO_FILE)) {
      logger.info("inside logAuditActivityToFile {}", entity.toString());
    }
  }


  /**
   * 
   * @param joinPoint
   */
  @After("annotationPointCutDefinition(auditable) && atExecution()")
  public void logAuditActivityToFile(JoinPoint joinPoint, Auditable auditable) {
    if (isAuditType(getAuditableObject(joinPoint), AuditType.PERSISTENCE_TO_FILE)) {
      logger.info("inside logAuditActivityToFile");
    }
  }

  /**
   * 
   * @param joinPoint
   * @return
   * @throws Throwable
   */
  @Around("annotationPointCutDefinition(auditable) && atExecution()")
  public Object logAuditActivityToDB(ProceedingJoinPoint joinPoint, Auditable auditable)
      throws Throwable {
    logger.info("inside logAuditActivityToDB {} ");

    Object[] arg0 = joinPoint.getArgs();
    Object object = joinPoint.proceed(arg0);

    // Auditable auditable = getAuditableObject(joinPoint);

    if (isAuditType(auditable, AuditType.PERSISTENCE_TO_DB)) {
      auditable.handler().newInstance().handler();
    }

    logger.info("end logAuditActivityToDB");
    return object;
  }

  /*
   * 
   * @param joinPoint
   * 
   * @return
   */
  private Auditable getAuditableObject(JoinPoint joinPoint) {
    MethodSignature signature = (MethodSignature) joinPoint.getSignature();
    Method method = signature.getMethod();

    Auditable auditable = method.getAnnotation(Auditable.class);
    return auditable;
  }

  /*
   * 
   * @param auditable
   * 
   * @param type
   * 
   * @return
   */
  private boolean isAuditType(Auditable auditable, AuditType type) {
    for (AuditType tp : auditable.values()) {
      if (tp.compareTo(type) == 0) {
        return true;
      }
    }
    return false;
  }
}
