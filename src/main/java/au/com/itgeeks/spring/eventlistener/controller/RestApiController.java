package au.com.itgeeks.spring.eventlistener.controller;


import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import au.com.itgeeks.spring.eventlistener.events.CustomerCreatedEvent;
import au.com.itgeeks.spring.eventlistener.listener.CustomerCreatedEventAsyncListener;
import au.com.itgeeks.spring.eventlistener.model.Customer;
import au.com.itgeeks.spring.eventlistener.service.CustomerCreatedEventListener;


@RestController
@RequestMapping("/api")
public class RestApiController {

  public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

  @Autowired
  private CustomerCreatedEventAsyncListener asyncListener;

  @Autowired
  private CustomerCreatedEventListener syncListener;

  @Autowired
  private ApplicationEventPublisher publisher;

  private ConcurrentMap<Long, Future<String>> concurrentMap =
      new ConcurrentHashMap<Long, Future<String>>();


  @RequestMapping(value = "/async", method = RequestMethod.GET)
  public ResponseEntity<?> asyncGreetings() {

    logger.info("{} received: ", Thread.currentThread().getName());

    Random rand = new Random();

    Long n = (long) (rand.nextInt(50) + 1);

    Customer cust = new Customer(n, "myName", "myemail@xyz.com");

    CustomerCreatedEvent event = new CustomerCreatedEvent(cust);

    asyncListener.processCustomerCreatedEvent(event);

    logger.info("{} finished: {} ", Thread.currentThread().getName(), event.getCustomer().getId());

    return new ResponseEntity<Object>("hello from publish aSync " + n, HttpStatus.OK);
  }

  @RequestMapping(value = "/pAsync", method = RequestMethod.GET)
  public ResponseEntity<?> publishedAsyncGreetings() {

    logger.info("{} received: ", Thread.currentThread().getName());

    Random rand = new Random();

    Long n = (long) (rand.nextInt(50) + 1);

    Customer cust = new Customer(n, "myName", "myemail@xyz.com");

    CustomerCreatedEvent event = new CustomerCreatedEvent(cust);

    publisher.publishEvent(event);

    logger.info("{} finished: {} ", Thread.currentThread().getName(), event.getCustomer().getId());

    return new ResponseEntity<Object>("hello from aSync " + n, HttpStatus.OK);
  }


  @RequestMapping(value = "/future", method = RequestMethod.GET)
  public ResponseEntity<?> asyncFutureGreetings() {

    logger.info("{} received: ", Thread.currentThread().getName());

    Random rand = new Random();

    Long n = (long) (rand.nextInt(50) + 1);

    Customer cust = new Customer(n, "myName", "myemail@xyz.com");

    CustomerCreatedEvent event = new CustomerCreatedEvent(cust);

    Future<String> future = asyncListener.asyncMethodWithReturnType(event);

    concurrentMap.put(n, future);

    logger.info("{} finished: {} ", Thread.currentThread().getName(), event.getCustomer().getId());

    return new ResponseEntity<Object>("hello from aSync future " + n, HttpStatus.OK);
  }

  @RequestMapping(value = "/futureResult", method = RequestMethod.GET)
  public ResponseEntity<?> asyncFutureResultGreetings(@RequestParam(value = "id") Long n) {

    if (!concurrentMap.isEmpty() && concurrentMap.containsKey(n)) {
      Future<String> future = concurrentMap.get(n);
      try {
        return new ResponseEntity<Object>(future.get(), HttpStatus.OK);
      } catch (InterruptedException | ExecutionException e) {
        return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
      }
    }
    return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
  }

  @RequestMapping(value = "/sync", method = RequestMethod.GET)
  public ResponseEntity<?> syncGreetings() {

    logger.info("{} received: ", Thread.currentThread().getName());

    Random rand = new Random();

    Long n = (long) (rand.nextInt(50) + 1);

    Customer cust = new Customer(n, "myName", "myemail@xyz.com");

    CustomerCreatedEvent event = new CustomerCreatedEvent(cust);

    syncListener.processCustomerCreatedEvent(event);

    logger.info("{} finished: {} ", Thread.currentThread().getName(), event.getCustomer().getId());

    return new ResponseEntity<Object>("hello from Sync " + n, HttpStatus.OK);
  }


}
