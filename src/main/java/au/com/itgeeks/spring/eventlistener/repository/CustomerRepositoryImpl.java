package au.com.itgeeks.spring.eventlistener.repository;

import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import au.com.itgeeks.spring.eventlistener.model.Customer;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

  @Override
  public List<Customer> findAll() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<Customer> findAll(Sort sort) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<Customer> findAll(Iterable<Long> ids) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public <S extends Customer> List<S> save(Iterable<S> entities) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void flush() {
    // TODO Auto-generated method stub
    
  }

  @Override
  public <S extends Customer> S saveAndFlush(S entity) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void deleteInBatch(Iterable<Customer> entities) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void deleteAllInBatch() {
    // TODO Auto-generated method stub
    
  }

  @Override
  public Customer getOne(Long id) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public <S extends Customer> List<S> findAll(Example<S> example) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public <S extends Customer> List<S> findAll(Example<S> example, Sort sort) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Page<Customer> findAll(Pageable arg0) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public long count() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public void delete(Long arg0) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void delete(Customer arg0) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void delete(Iterable<? extends Customer> arg0) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void deleteAll() {
    // TODO Auto-generated method stub
    
  }

  @Override
  public boolean exists(Long arg0) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public Customer findOne(Long arg0) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public <S extends Customer> S save(S arg0) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public <S extends Customer> long count(Example<S> arg0) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public <S extends Customer> boolean exists(Example<S> arg0) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public <S extends Customer> Page<S> findAll(Example<S> arg0, Pageable arg1) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public <S extends Customer> S findOne(Example<S> arg0) {
    // TODO Auto-generated method stub
    return null;
  }

}
