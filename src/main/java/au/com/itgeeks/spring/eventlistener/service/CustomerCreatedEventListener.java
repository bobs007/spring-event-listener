package au.com.itgeeks.spring.eventlistener.service;

import au.com.itgeeks.spring.eventlistener.events.CustomerCreatedEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 
 * @author bobs
 *
 */
@Component
public class CustomerCreatedEventListener {

  private static final Logger LOGGER = LoggerFactory.getLogger(CustomerCreatedEventListener.class);

  private final TokenGenerator tokenGenerator;

  public CustomerCreatedEventListener(TokenGenerator tokenGenerator) {
    this.tokenGenerator = tokenGenerator;
  }

  @TransactionalEventListener
  public void processCustomerCreatedEvent(CustomerCreatedEvent event) {
    LOGGER.info("{} Event received: {} ", Thread.currentThread().getName(),
        event.getCustomer().getId());
    tokenGenerator.generateToken(event.getCustomer());
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    LOGGER.info("{} Event finished: {} ", Thread.currentThread().getName(),
        event.getCustomer().getId());
  }
}
