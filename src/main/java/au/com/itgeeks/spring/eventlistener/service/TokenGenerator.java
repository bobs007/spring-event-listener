package au.com.itgeeks.spring.eventlistener.service;

import au.com.itgeeks.spring.eventlistener.model.Customer;

/**
 * 
 * @author bobs
 *
 */
public interface TokenGenerator {

  void generateToken(Customer customer);
}
