package au.com.itgeeks.spring.eventlistener.audit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

@Qualifier
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Transactional
public @interface Auditable {

  public enum AuditType {
    PERSISTENCE_TO_DB, PERSISTENCE_TO_FILE, PERSISTENCE_TO_MEDIA
  }

  AuditType[] values() default AuditType.PERSISTENCE_TO_FILE;

  Class<? extends AuditHandler> handler();
  
  String auditString() default "";
}
