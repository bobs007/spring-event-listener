package au.com.itgeeks.spring.eventlistener.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import au.com.itgeeks.spring.eventlistener.service.SomeService;

@RestController
@RequestMapping("/api")
public class AuditApis {
  
  public static final Logger logger = LoggerFactory.getLogger(AuditApis.class);
  
  @Autowired
  private SomeService service;
  
  @RequestMapping(value = "/audit", method = RequestMethod.GET)
  public ResponseEntity<?> doAudit() {
    logger.info("inside doAudit() {}  : ", Thread.currentThread().getName());
    //
    service.someMethod(1);
    logger.info("end doAudit() {} : ", Thread.currentThread().getName());
    return null;
  }

}
