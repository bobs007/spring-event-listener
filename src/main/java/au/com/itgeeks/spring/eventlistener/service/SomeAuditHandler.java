package au.com.itgeeks.spring.eventlistener.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import au.com.itgeeks.spring.eventlistener.audit.AuditHandler;

@Service
public class SomeAuditHandler implements AuditHandler {

  public static final Logger logger = LoggerFactory.getLogger(SomeAuditHandler.class);

  @Override
  public void handler() {
    logger.info("doing audit");
  }

}
