package au.com.itgeeks.spring.eventlistener.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.itgeeks.spring.eventlistener.audit.Auditable;
import au.com.itgeeks.spring.eventlistener.audit.Auditable.AuditType;

@Service
@Transactional 
public class SomeService {

  public static final Logger logger = LoggerFactory.getLogger(SomeService.class);
  
  @Auditable(values = {AuditType.PERSISTENCE_TO_FILE}, handler = SomeAuditHandler.class)
  public void someMethod(final int number) {
    final int newNumber = number + 1;
    
    logger.info("inside someMethod {}", newNumber);
  }
  
}
