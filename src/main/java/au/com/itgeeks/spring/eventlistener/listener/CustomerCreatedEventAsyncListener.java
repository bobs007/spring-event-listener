package au.com.itgeeks.spring.eventlistener.listener;

import au.com.itgeeks.spring.eventlistener.events.CustomerCreatedEvent;
import au.com.itgeeks.spring.eventlistener.service.TokenGenerator;

import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 
 * @author bobs
 *
 */
@Component
@Scope("prototype")
public class CustomerCreatedEventAsyncListener {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(CustomerCreatedEventAsyncListener.class);

  private final TokenGenerator tokenGenerator;

  public CustomerCreatedEventAsyncListener(TokenGenerator tokenGenerator) {
    this.tokenGenerator = tokenGenerator;
  }

  @TransactionalEventListener
  @Async
  public void processCustomerCreatedEvent(CustomerCreatedEvent event) {
    LOGGER.info("{} Event received: {} ", Thread.currentThread().getName(),
        event.getCustomer().getId());
    tokenGenerator.generateToken(event.getCustomer());
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    LOGGER.info("{} Event finshed: {} ", Thread.currentThread().getName(),
        event.getCustomer().getId());
  }

  @TransactionalEventListener
  @Async
  public Future<String> asyncMethodWithReturnType(CustomerCreatedEvent event) {
    LOGGER.info("{} Event received: {} ", Thread.currentThread().getName(),
        event.getCustomer().getId());
    try {
      Thread.sleep(30000);
    } catch (InterruptedException e) {
    }
    LOGGER.info("{} Event finshed: {} ", Thread.currentThread().getName(),
        event.getCustomer().getId());
    return new AsyncResult<String>("In future mode : hello from " + event.getCustomer().getId());
  }
}
