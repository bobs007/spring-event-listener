package au.com.itgeeks.spring.eventlistener.service;

import au.com.itgeeks.spring.eventlistener.model.Customer;
import au.com.itgeeks.spring.eventlistener.repository.CustomerRepository;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("async")
public class AsyncCallTokenGenerator implements TokenGenerator {

  private final CustomerRepository customerRepository;

  public AsyncCallTokenGenerator(CustomerRepository customerRepository) {
    this.customerRepository = customerRepository;
  }

  public void generateToken(Customer customer) {
    final String token = String.valueOf(customer.hashCode());
    customer.activatedWith(token);
    customerRepository.save(customer);
  }
}
