package au.com.itgeeks.spring.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
// @EnableAsync
@ComponentScan("au.com.itgeeks.*")
public class EventListenerApplication {

  public static void main(String[] args) {
    SpringApplication.run(EventListenerApplication.class, args);
  }

}
